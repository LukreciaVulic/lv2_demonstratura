﻿using System;

namespace zadatak2
{
    class Program
    {
        static void testFunction()
        {
            RandomGenerator randomGenerator = new RandomGenerator();
            _3DVector vector1 = randomGenerator.generateRandomVector(5, 10);
            _3DVector vector2 = randomGenerator.generateRandomVector(15, 20);

            _3DVector vectorPlus = vector1 + vector2;
            _3DVector vectorMinus = vector1 - vector2;

            Console.WriteLine(vector1.ToString());
            Console.WriteLine(vector2.ToString());
            Console.WriteLine(vectorPlus.ToString());
            Console.WriteLine(vectorMinus.ToString());

        }
        static void Main(string[] args)
        {
            testFunction();
        }
    }
}
