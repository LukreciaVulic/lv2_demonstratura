﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
    public class _3DVector
    {
        private double x;
        private double y;
        private double z;

        public double X { get => x; set => x = value; }
        public double Z { get => z; set => z = value; }
        public double Y { get => y; set => y = value; }

        public _3DVector(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public _3DVector()
        {
            this.X = 0;
            this.Y = 0;
            this.Z = 0;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("x = ");
            stringBuilder.Append(this.x);
            stringBuilder.Append("\t");
            stringBuilder.Append("y = ");
            stringBuilder.Append(this.y);
            stringBuilder.Append("\t");
            stringBuilder.Append("z = ");
            stringBuilder.Append(this.z);
            return stringBuilder.ToString();
        }

        public static _3DVector operator +(_3DVector vector1, _3DVector vector2)
        {
            return new _3DVector(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z);
        }

        public static _3DVector operator -(_3DVector vector1, _3DVector vector2)
        {
            return new _3DVector(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z);
        }
    }
}

