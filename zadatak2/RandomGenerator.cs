﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
    public class RandomGenerator
    {
        private Random random= new Random();
        
        public _3DVector generateRandomVector(int lowerBound, int upperBound)
        {
            _3DVector vector = new _3DVector();
            vector.X = (random.NextDouble() * (upperBound - lowerBound)) + lowerBound;
            vector.Y = (random.NextDouble() * (upperBound - lowerBound)) + lowerBound;
            vector.Z = (random.NextDouble() * (upperBound - lowerBound)) + lowerBound;
            return vector;
        }
    }
}
