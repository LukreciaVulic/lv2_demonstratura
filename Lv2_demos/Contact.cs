﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv2_demos
{
    public class Contact
    {
        string name;
        string lastName;
        string phoneNumber;
        string eMail;

        public Contact(string name, string lastName, string phoneNumber, string eMail)
        {
            this.Name = name;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
            this.EMail = eMail;
        }

        public string Name { get => name; set => name = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string EMail { get => eMail; set => eMail = value; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(name);
            stringBuilder.Append("\t");
            stringBuilder.Append(lastName);
            stringBuilder.Append("\t");
            stringBuilder.Append(phoneNumber);
            stringBuilder.Append("\t");
            stringBuilder.Append(eMail);
            return stringBuilder.ToString();

        }
    }
    
}
