﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv2_demos
{
    public static class EmailHelper
    {
        static public bool isEmail(string emailCandidate)
        {
            if (emailCandidate.Contains('@') && (emailCandidate.EndsWith("gmail.com") || emailCandidate.EndsWith("ferit.hr")))
            {
                return true;
            }
            return false;
        }

        static public bool isFeritEmail(string emailCandidate)
        {
            if (emailCandidate.EndsWith("ferit.hr"))
            {
                return true;
            }
            return false;
        }
    }
}
