﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak3
{
    class DoctorAppointment
    {
        private string patientName;
        DateTime time;
        TimeSpan duration;

        public string PatientName { get => patientName;  }
        public DateTime Time { get => time; }
        public TimeSpan Duration { get => duration; }

        public DoctorAppointment(string patientName, DateTime time, TimeSpan duration)
        {
            this.patientName = patientName;
            this.time = time;
            this.duration = duration;
        }

        public void postponeAppointment(DateTime newTime)
        {
            time = newTime;
        }

        public static bool operator >(DoctorAppointment docAp1, DoctorAppointment docAp2)
        {
            if (DateTime.Compare(docAp1.Time,docAp2.Time)>0) return true;
            return false;
        }
        public static bool operator <(DoctorAppointment docAp1, DoctorAppointment docAp2)
        {
            if (DateTime.Compare(docAp1.Time, docAp2.Time) < 0) return true;
            return false;
        }

        public override string ToString()
        {
            return PatientName;
        }
    }
}
