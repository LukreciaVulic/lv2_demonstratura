﻿using System;

namespace zadatak3
{
    class Program
    {
        static TimeSpan testFunction(DoctorAppointment[] doctorAppointmens, int size)
        {
            DoctorAppointment temp;

            for(int i = 0; i < size-1; i++)
            {
                for(int j = 0; j < size-i-1; j++)
                {
                    if (doctorAppointmens[j] > doctorAppointmens[j + 1])
                    {
                        temp = doctorAppointmens[j];
                        doctorAppointmens[j] = doctorAppointmens[j + 1];
                        doctorAppointmens[j + 1] = temp;

                    }
                }
            }

            for(int i = 0; i < size; i++){
                Console.WriteLine(doctorAppointmens[i].ToString());
            }
            TimeSpan minDuration = doctorAppointmens[1].Time-doctorAppointmens[0].Duration - doctorAppointmens[0].Time;

            for (int i = 0; i < size-1; i++)
            {
                if (doctorAppointmens[i+1].Time - doctorAppointmens[i].Duration - doctorAppointmens[i].Time < minDuration)
                    minDuration = doctorAppointmens[i+1].Time - doctorAppointmens[i].Duration - doctorAppointmens[i].Time;
            }

            return minDuration;

        }
        static void Main(string[] args)
        {
            DoctorAppointment docAp1 = new DoctorAppointment("Ivo Ivic", new DateTime(2020, 10, 15, 8, 30, 0), new TimeSpan(0,30,0));
            DoctorAppointment docAp2 = new DoctorAppointment("Marko Marković", new DateTime(2020, 10, 15, 9, 30, 0), new TimeSpan(0,45,0));
            DoctorAppointment docAp3 = new DoctorAppointment("Zlatko Zlatan", new DateTime(2020, 10, 15, 10, 30, 0), new TimeSpan(0,30,0));

            DoctorAppointment[] array = new DoctorAppointment[3];
            array[0] = docAp3;
            array[1] = docAp1;
            array[2] = docAp2;

            TimeSpan timeSpan = testFunction(array, 3);
            Console.WriteLine(timeSpan.TotalMinutes);
        }
    }
}
